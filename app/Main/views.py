import re

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from app.Articls.models import Articls, Projects, Img, Tags
from .forms import UploadFileForm
from zipfile import ZipFile
import os
import shutil
from PIL import Image
import random
import string
import markdown
from django.conf import settings
from .ocxsect import OCXMetadata



from math import log
def get_histogram_dispersion(histogram):
    log2 = lambda x:log(x)/log(2)

    total = len(histogram)
    counts = {}
    for item in histogram:
        counts.setdefault(item,0)
        counts[item]+=1

    ent = 0
    for i in counts:
        p = float(counts[i])/total
        ent -= p*log2(p)
    return -ent*log2(1/ent)


def resize_image(input_image_path,
                 output_image_path):

    original_image = Image.open(input_image_path)
    (width, height) = original_image.size
    size = (width//50, height//50)
    res_image = original_image.resize((100, 100))
    res_path = input_image_path[:input_image_path.find(".")] + '-resize' + input_image_path[input_image_path.find("."):]
    res_image.save(res_path)


    # Вычисляем энтропию изображения
    im = Image.open(res_path)
    h = im.histogram()
    os.remove(res_path)

    #Определяем инфографику
    # if round(get_histogram_dispersion(h), 1) < 11:
    #     original_image_path = output_image_path[:output_image_path.find(".")] + '-info' + output_image_path[output_image_path.find("."):]
    #     print(output_image_path)
    #     original_image.save(original_image_path)
    #
    #     # Удаляем из пути все директории пока не останеться только имя файла
    #     while '/' in original_image_path:
    #         original_image_path = original_image_path[original_image_path.find('/') + 1:]
    #
    #     return original_image_path
    # else:
    #     resized_image = original_image.resize(size)
    #     resized_image_path = output_image_path[:output_image_path.find(".")] + output_image_path[output_image_path.find("."):]
    #     resized_image.save(resized_image_path)
    #
    #     # Удаляем из пути все директории пока не останеться только имя файла
    #     while '/' in resized_image_path:
    #         resized_image_path = resized_image_path[resized_image_path.find('/')+1:]
    #
    #     return resized_image_path


    original_image_path = output_image_path[:output_image_path.find(".")] + '-info' + output_image_path[output_image_path.find("."):]
    original_image.save(original_image_path)

    resized_image = original_image.resize(size)
    resized_image_path = output_image_path[:output_image_path.find(".")] + output_image_path[output_image_path.find("."):]
    resized_image.save(resized_image_path)

    # Удаляем из пути все директории пока не останеться только имя файла
    while '/' in original_image_path:
        original_image_path = original_image_path[original_image_path.find('/') + 1:]

    return original_image_path



def genDir(size):
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(size))

def extract_zip(f, title, subtitle, project, tags):
    dir = settings.STORAGE_ROOT + '/' + f.name[: f.name.find(".")]
    os.mkdir(dir)
    # Распаковка архива
    with ZipFile('media/files/' + f.name, 'r') as zip:
        zip.extractall(path=dir)

    # Перекодировка названий файлов
    for root, dirs, files in os.walk(dir):
        for filename in files:
            name = dir + '/' + filename
            os.rename(name, name.encode('cp437').decode('cp866'))

    # Создание статьи на основе содержимого архива
    articl = Articls()
    articl.uid = f.name[: f.name.find(".")]
    pr = Projects.objects.all()
    articl.project = pr[int(project)-1]  # Передаем ID проекта для статьи
    tg = Tags.objects.all()
    # Поиск файла с маркдаун разметкой
    for root, dirs, files in os.walk(dir):
        for filename in files:
            # Если встречаем файл с расширение *.md, читаем и содержимое записываем в поле модели text
            if filename[filename.find(".") + 1:] == 'md':
                with open(dir + '/' + filename, 'r', encoding="utf-8") as md:
                    articl.text = md.read()
                    articl.title = title
                    articl.subtitle = subtitle


    # Сохраняем объект(статью) в БД
    articl.save()
    for tag in tags:
        articl.tags.add(tg[int(tag)-1])

    # Поиск изображений
    expansion_image = ['jpg', 'jpeg', 'png', 'gif', 'ico', 'svg']
    for root, dirs, files in os.walk(dir):
        for filename in files:
            # Если встречаем файл с расширение изображения, читаем и записываем в поле модели imeg
            if filename[filename.find(".") + 1:] in expansion_image:
                with open(dir + '/' + filename, 'r') as jpg:
                    if filename[:filename.find(".")] == 'banner':
                        articl.banner = 'img/' + resize_image(dir + '/' + filename, "media/img/" + filename)
                        articl.save()
                    else:
                        img = Img()
                        img.img = 'img/' + resize_image(dir + '/' + filename, dir + "/" + filename)
                        img.articls = Articls.objects.get(title=articl.title)
                        img.save()


    #Удаление директории
    # shutil.rmtree(dir, ignore_errors=False, onerror=None)
    #Удаление архива
    os.remove('media/files/' + f.name)


def delete(f):
    dir = 'media/files/' + f.name[: f.name.find(".")]
    shutil.rmtree(dir, ignore_errors=False, onerror=None)
    os.remove('media/files/' + f.name)
# Скачивание архива
def save_file(f):
    with open('media/files/' + f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)



def index(request):
    articls = Articls.objects.all()
    imgs = Img.objects.all()
    tags = Tags.objects.all()
    if request.POST:
        # try:
            file = UploadFileForm(request.POST, request.FILES)
            if file.is_valid():
                title = request.POST['title']
                subtitle = request.POST['subtitle']
                project = request.POST['project']
                tags = request.POST.getlist('check')

                save_file(request.FILES['file'])
                extract_zip(request.FILES['file'], title, subtitle, project, tags)
                return HttpResponseRedirect('/#')
        # except:
        #     # Если загружен файл не формата ZIP или ошибка в чтении удалить созданную копию и деректорию
        #     delete(request.FILES['file'])
        #     return HttpResponseRedirect('/#')
    else:
        form = UploadFileForm()
    return render(request, 'Main/index.html', {'articls': articls, 'imgs': imgs, 'tags': tags, 'form': form})










# Переводит md файл в html
def convert_to_html_toc(doc):
    md = markdown.Markdown(extensions=['toc', 'tables', 'footnotes', 'fenced_code', OCXMetadata()])
    dir = settings.STORAGE_ROOT + '/' + doc + '/' + 'index.md'
    print(dir)
    with open(dir, 'r', encoding="utf-8") as file:
        md_text = file.read()
    # md_text = (settings.STORAGE_ROOT + '/' + doc + '/' + 'index.md').read_text(encoding='UTF-8')

    md_text = md_text.replace('\\</=', '&le;')
    md_text = md_text.replace('\\<', '&lt;')
    md_text = md_text.replace('\\>/=', '&ge;')
    md_text = md_text.replace('\\>', '&gt;')

    html = md.convert(md_text)

    return html




def add_form(uid):
    return """
                   <p><input name="img" type="radio" value="photo">Фотография</p>
                   <p><input name="img" type="radio" value="info">Инфографика</p>
                   <p><input type="submit" onclick="changeImg('""" + uid + """')" value="Выбрать"></p>
                   """


# Изменяем html переведенный из md файл
def get_context_data(dir):
    h1_template = '<div class="row"><div class="col w100"><div id="content0" class="journal-item mt-0"><h1 {}</div></div></div><div class="gray-line"></div>'

    html = convert_to_html_toc(dir)


    h1 = [h1_template.format(x.strip()) for x in html.split('<h1') if x.strip() != '']
    html = '\n'.join(h1)
    html = html.replace('<div>***</div>',
                        '<div style="text-align: center"><img src="/static/img/sp/stars.png"/></div>')
    html = re.sub(r'<img alt=""\s+src="stars"\s*/?>',
                  '<div style="text-align: center"><img class="original-size" src="/static/img/sp/stars.png"></div>',
                  html)
    html = re.sub(r'<img alt=""\s+src="sp"\s*/?>', '<img class="original-size" src="/static/img/sp/sp.png"/>', html)
    html = re.sub(r'<img alt="([^"]*)" src="([^"]+)">',
                  '<a href="\\2" data-lightbox="\\1"><img src="\\2" alt="\\1"/></a', html)




    html = re.sub(r'(href|src)="([-_0-9a-zA-Z.а-яА-Я]+)"', '\\1="/media/storage/1/{}/\\2"'.format(dir), html)

    # html = re.sub(r'<img ([^<]*)', '<img id="{}" \\1<br><br>'.format(dir) + add_form(dir), html)
    html = re.sub(r'<img alt="([^"]*)" src="([^"]+)([^<]*)', '<img id="\\2" alt="\\1" src="\\2" \\3<br><br>' + """
                   <p><input name="img" type="radio" value="photo">Фотография</p>
                   <p><input name="img" type="radio" value="info">Инфографика</p>
                   <p><input type="submit" onclick="changeImg('\\2')" value="Выбрать"></p>
                   """, html)



    html = html.replace('<a class="footnote-backref"', ' <a class="footnote-backref"')
    html = html.replace('<table', '<div style="overflow:auto"><table')
    html = html.replace('</table>', '</table></div>')


    html = re.sub(r'(<[^>]+spid="(\d+)")', r'<a name="spid\2"/>\1', html)


    return html




def post(request, pk):
    html = get_context_data(pk)
    context = {
        # 'articl': Articls.objects.get(id=pk),
        'html': html,
    }
    return render(request, 'Main/post.html', context)




