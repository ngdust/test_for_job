from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='main'),
    url(r'post/(?P<pk>[\w|\W]+)/$', views.post, name='post'),
]
