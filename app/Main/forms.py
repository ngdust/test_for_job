from django import forms
from app.Articls.models import Projects

class UploadFileForm(forms.Form):
    project = forms.ModelChoiceField(Projects.objects.all(), empty_label='Проект')
    title = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Заголовок статьи'}))
    subtitle = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Подзаголовок статьи'}))
    file = forms.FileField()