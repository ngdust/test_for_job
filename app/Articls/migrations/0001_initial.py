# Generated by Django 3.0.2 on 2020-01-20 02:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categorys',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Названия')),
            ],
            options={
                'verbose_name': 'Категории статей',
                'verbose_name_plural': 'Категории статей',
            },
        ),
        migrations.CreateModel(
            name='Projects',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Названия')),
            ],
            options={
                'verbose_name': 'Проекты',
                'verbose_name_plural': 'Проекты',
            },
        ),
        migrations.CreateModel(
            name='Tags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=10, verbose_name='Код тега')),
                ('name', models.CharField(max_length=50, verbose_name='Названия')),
                ('sm_name', models.CharField(max_length=10, verbose_name='Короткое название')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Articls.Categorys', verbose_name='Категория')),
            ],
            options={
                'verbose_name': 'Теги',
                'verbose_name_plural': 'Теги',
            },
        ),
        migrations.CreateModel(
            name='Articls',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
                ('text', models.TextField(verbose_name='Текст статьи')),
                ('image', models.ImageField(upload_to='', verbose_name='Картинка')),
                ('banner', models.ImageField(upload_to='', verbose_name='Баннер')),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Articls.Projects', verbose_name='Проект')),
                ('tags', models.ManyToManyField(to='Articls.Tags', verbose_name='Теги')),
            ],
            options={
                'verbose_name': 'Статьи',
                'verbose_name_plural': 'Статьи',
            },
        ),
    ]
