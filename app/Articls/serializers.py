from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from .models import Articls, Tags, Categorys, Projects, Img

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Categorys
        fields = ('name',)

class TagsSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Tags
        fields = ('code', 'name', 'category')

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ('name',)

class ImgSerializer(serializers.ModelSerializer):
    articls = serializers.PrimaryKeyRelatedField(queryset=Articls.objects.all(), source='articls.id')

    class Meta:
        model = Img
        fields = ('articls', 'img')

    def create(self, validated_data):
        return Img.objects.create(**validated_data)


class ArticleSerializer(serializers.ModelSerializer):
    tags = TagsSerializer(many=True, read_only=True)
    project = PrimaryKeyRelatedField(queryset=Projects.objects.all())
    img = ImgSerializer(many=True, read_only=True)

    class Meta:
        model = Articls
        fields = ('id', 'project', 'title', 'subtitle', 'text', 'banner', 'img', 'tags')

    def create(self, validated_data):
        return Articls.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.project = validated_data.get('project', instance.project)
        instance.title = validated_data.get('title', instance.title)
        instance.text = validated_data.get('text', instance.text)
        instance.save()
        return instance