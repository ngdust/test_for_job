from django.contrib import admin
from .models import Projects, Categorys, Tags, Articls, Img


# Register your models here.

class Image(admin.TabularInline):
    extra = 1
    model = Img
    classes = ['collapse']


class Articls_In_Admin(admin.ModelAdmin):
    inlines = [Image]


admin.site.register(Projects)
admin.site.register(Categorys)
admin.site.register(Tags)
admin.site.register(Articls, Articls_In_Admin)

