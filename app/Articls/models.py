from django.db import models
from markdown import markdown

# Create your models here.
class Projects(models.Model):
    name = models.CharField(max_length=100, verbose_name='Названия')

    class Meta:
        verbose_name = 'Проекты'
        verbose_name_plural = "Проекты"

    def __str__(self):
        return self.name

class Categorys(models.Model):
    name = models.CharField(max_length=100, verbose_name='Названия')

    class Meta:
        verbose_name = 'Категории статей'
        verbose_name_plural = "Категории статей"

    def __str__(self):
        return self.name

class Tags(models.Model):
    code = models.CharField(max_length=10, verbose_name='Код тега')
    name = models.CharField(max_length=50, verbose_name='Названия')
    sm_name = models.CharField(max_length=10, verbose_name='Короткое название')
    category = models.ForeignKey(Categorys, on_delete=models.CASCADE, verbose_name='Категория')

    class Meta:
        verbose_name = 'Теги'
        verbose_name_plural = "Теги"

    def __str__(self):
        return self.name


class Articls(models.Model):
    project = models.ForeignKey(Projects, on_delete=models.CASCADE,  verbose_name='Проект')
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    subtitle = models.CharField(max_length=200, default='',verbose_name='Подзаголовок')
    text = models.TextField(verbose_name='Текст статьи')
    banner = models.ImageField(upload_to='img', blank=True, verbose_name='Баннер')
    tags = models.ManyToManyField(Tags, verbose_name='Теги')
    uid = models.CharField(max_length=100, verbose_name='UID', default=0)

    class Meta:
        verbose_name = 'Статьи'
        verbose_name_plural = "Статьи"

    def __str__(self):
        return self.title

    #Переопределяем метод сохранения, чтобы текст статьи преобразовывался в разметку MARKDOWN
    def save(self, *args, **kwargs):
        self.text = markdown(self.text)
        super(Articls, self).save(*args, **kwargs)

class Img(models.Model):
    articls = models.ForeignKey(Articls, on_delete=models.CASCADE, verbose_name='Статья', related_name='img')
    img = models.ImageField(upload_to='img', blank=True, verbose_name='Картинка')

    def __str__(self):
        return self.articls.title + self.img.name


